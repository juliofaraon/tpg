<?php

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

Route::get('/', 'Dashboard@index')->name('admin_dashboard');

Route::get('/events', function () {
    return 'EVENTOS';
})->name('admin_events');

Route:: catch(function () {
    throw new NotFoundHttpException();
});

Route::get('search-user', function (Request $request) {
    $pattern = '/[A-Za-z][A-Za-z ]+/';
    $query = $request->get('query');
    if (preg_match($pattern, $query)) {
        return response(App\User::where('name', 'like', "%$query%")->select('name', 'id')->get());
    } else {
        return response()->json([]);
    }
})->name('search-user');

Route::resource('/slides', 'SlideController', ['except' => 'show']);
Route::resource('/menus', 'MenuController', ['except' => 'show']);
Route::resource('/roles', 'RolesController');
Route::resource('/abilities', 'AbilitiesController', ['except' => 'show']);

Route::put('/roles-menu/{role}', 'RolesController@menu')->name('roles.menu')->middleware('can:role-menu');
Route::put('/roles-abilities/{role}', 'RolesController@abilities')->name('roles.ability')->middleware('can:role-ability');

Route::resource('/user', 'UserController');
Route::any('/user-change-password/{user}', 'UserController@changePassword')->name('change-password-user')->middleware('can:password-user');
Route::any('/user-change-role/{user}', 'UserController@changeRole')->name('change-role-user')->middleware('can:role-user');

Route::resource('/admin', 'AdminController');
Route::any('/admin-change-password/{admin}', 'AdminController@changePassword')->name('change-password-admin')->middleware('can:password-admin');
Route::any('/admin-change-role/{admin}', 'AdminController@changeRole')->name('change-role-admin')->middleware('can:role-admin');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

