let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/scss/colors/blue.scss', 'public/css');
mix.sass('resources/assets/scss/colors/green-dark.scss', 'public/css');


mix.scripts([
    'public/js/app.js',
    'resources/assets/js/custom/jquery.slimscroll.js',
    'resources/assets/js/custom/sidebarmenu.js',
    'resources/assets/js/custom/sticky-kit.min.js',
    'resources/assets/js/custom/waves.js',
    'resources/assets/js/custom/stacktable.js',
    'resources/assets/js/custom/coreTemplate.js',
    'resources/assets/js/custom/utils.js',
], 'public/js/app.js');

mix.copy([
'resources/assets/images/logo.png',
'resources/assets/images/info.png',
'resources/assets/images/user.png',
'resources/assets/images/userinfo.jpg',
'resources/assets/images/logo/logo-icon.png',
'resources/assets/images/logo/logo-light-text.png',
'resources/assets/images/logo/logo-text.png',
],'public/images');

mix.copyDirectory([
    'resources/assets/images/favicon',
    ],'public/images');