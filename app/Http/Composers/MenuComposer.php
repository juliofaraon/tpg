<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Core\Eloquent\Security\Menu;

class MenuComposer
{
    public function compose(View $view)
    {
        $view->menus = Menu::menus(auth()->user()->roles()->pluck('id')->toArray(), 'join');
    }
}
