<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Admin;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use App\Http\Controllers\Controller;
use App\Core\Repositories\SecurityRPY;
use App\Http\Requests\Admin\AdminRequest;
use Facades\App\Core\Contracts\TableRepository;
use Hash;

class AdminController extends Controller
{
    private $objSecurityRPY;

    public function __construct(SecurityRPY $objSecurityRPY)
    {
        $this->abilityCRUD('admin', true);
        $this->objSecurityRPY = $objSecurityRPY;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.users.admins')->with([
            'filter' => $request->filter,
            'admins' => TableRepository::forAdmins($request->filter),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create')->with([
            'roles' => $this->objSecurityRPY->roles(), 'type' => 'admin',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        DB::transaction(function () use (&$request) {
            $this->objSecurityRPY->createUserOrAdminAndRole(new Admin(), $request->validated(), $request->role, true);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Admin $admin
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admin.users.show')->with(['type' => 'admin', 'user' => $admin]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Admin $admin
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.users.edit')->with([
            'roles' => $this->objSecurityRPY->roles(), 'type' => 'admin', 'user' => $admin,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Admin               $admin
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRequest $request, Admin $admin)
    {
        DB::transaction(function () use (&$request, &$admin) {
            $this->objSecurityRPY->createUserOrAdminAndRole($admin, $request->validated(), $request->role, false);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Admin $admin
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->status = $admin->status == 'A' ? 'I' : 'A';
        $admin->save();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('admin.index');
    }

    public function changePassword(Request $request, Admin $admin)
    {
        if ($request->method() == 'GET') {
            return view('admin.users.password')->with(['password' => str_random(6), 'user' => $admin, 'type' => 'admin']);
        } else {
            $this->validate($request, ['password' => 'required|min:6']);
            $admin->password = ($request->password);
            $admin->save();
            session()->flash('status', __('Proccess OK'));

            return redirect()->route('admin.index');
        }
    }

    public function changeRole(Request $request, Admin $admin)
    {
        if ($request->method() == 'GET') {
            return view('admin.users.role')->with(['roles' => $this->objSecurityRPY->roles(), 'user' => $admin, 'type' => 'admin']);
        } else {
            $this->validate($request, ['role' => 'required|exists:bouncer_roles,id']);
            $admin->retract($admin->roles()->get());
            $admin->assign(Role::find($request->role));
            session()->flash('status', __('Proccess OK'));

            return redirect()->route('admin.index');
        }
    }
}
