<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use App\Http\Controllers\Controller;
use App\Core\Repositories\SecurityRPY;
use App\Http\Requests\Admin\UserRequest;
use Facades\App\Core\Contracts\TableRepository;
use DB;
use Hash;
class UserController extends Controller
{
    private $objSecurityRPY;

    public function __construct(SecurityRPY $objSecurityRPY)
    {
        $this->abilityCRUD('user', true);
        $this->objSecurityRPY = $objSecurityRPY;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.users.users')->with(['filter' => $request->filter,
        'users' => TableRepository::forUsers($request->filter), ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create')->with([
            'roles' => $this->objSecurityRPY->roles(), 'type' => 'user', ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::transaction(function () use (&$request) {
            $this->objSecurityRPY->createUserOrAdminAndRole(new User(), $request->validated(), $request->role, true);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show')->with(['type' => 'user', 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit')->with([
            'roles' => $this->objSecurityRPY->roles(), 'user' => $user, 'type' => 'user', ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        DB::transaction(function () use (&$request,&$user) {
            $this->objSecurityRPY->createUserOrAdminAndRole($user, $request->validated(), $request->role, false);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->status = $user->status == 'A' ? 'I' : 'A';
        $user->save();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('user.index');
    }

    public function changePassword(Request $request, User $user)
    {
        if ($request->method() == 'GET') {
            return view('admin.users.password', compact('user'))->with(['password' => str_random(6), 'type' => 'user']);
        } else {
            $this->validate($request, ['password' => 'required|min:6']);
            $user->password =$request->password;
            $user->save();
            session()->flash('status', __('Proccess OK'));

            return redirect()->route('user.index');
        }
    }

    public function changeRole(Request $request, User $user)
    {
        if ($request->method() == 'GET') {
            return view('admin.users.role', compact('user'))->with(['roles' => $this->objSecurityRPY->roles(), 'type' => 'user']);
        } else {
            $this->validate($request, ['role' => 'required|exists:bouncer_roles,id']);
            $user->retract($user->roles()->get());
            $user->assign(Role::find($request->role));
            session()->flash('status', __('Proccess OK'));

            return redirect()->route('user.index');
        }
    }
}
