<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function abilityCRUD($name, $flag = false)
    {
        $this->middleware("can:index-$name", ['only' => ['index']]);
        $this->middleware("can:create-$name", ['only' => ['create', 'store']]);
        $this->middleware("can:update-$name", ['only' => ['update', 'edit']]);
        if ($flag) {
            $this->middleware("can:show-$name", ['only' => ['show']]);
        }
        $this->middleware("can:delete-$name", ['only' => ['delete']]);
    }
}
