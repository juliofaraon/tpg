<?php

namespace App\Http\Controllers;

use App\User;
use App\Admin;
use Illuminate\Http\Request;
use App\Core\Eloquent\DatabaseNotification;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ContactMailNotification;

class NotificationsController extends Controller
{
    public function getAllNotifications(Request $request)
    {
        $notifications = currentUser()->notifications()->paginate(50);

        return view('notifications', compact('notifications'));
    }

    public function getNotification(DatabaseNotification $notification)
    {
        abort_unless($notification->associatedTo(currentUser()), 404);
        $notifications = currentUser()->notifications()->where('id', '=', $notification->id)->paginate(50);
        $notification->markAsRead();

        return view('notifications', compact('notifications'));
    }

    public function readAllNotifications()
    {
        currentUser()->unreadNotifications->markAsRead();

        return redirect()->route('getAllNotification');
    }

    public function check(DatabaseNotification $notification)
    {
        abort_unless($notification->associatedTo(currentUser()), 404);
        $notification->markAsRead();

        return redirect()->route('getAllNotification');
    }

    public function uncheck(DatabaseNotification $notification)
    {
        abort_unless($notification->associatedTo(currentUser()), 404);
        $notification->markAsUnread();

        return redirect()->route('getAllNotification');
    }

    public function sendNotifications(Request $request)
    {
        if (auth()->user()->isAdmin()) {
            $this->validate($request, ['contactDescription' => 'required|max:150', 'destine.id' => 'required|exists:users,id']);
            Notification::send(User::find($request->destine['id']), (new ContactMailNotification(currentUser(), $request->only('contactDescription'))));
        } else {
            $this->validate($request, ['contactDescription' => 'required|max:150']);
            Notification::send(Admin::get(), (new ContactMailNotification(currentUser(), $request->only('contactDescription'))));
        }

        return response()->json(['result' => __('notifications.send')]);
    }

    public function searchNotification()
    {
        return response()->json([
            'render' => view('partials.notifications')->render(),
      ]);
    }
}
