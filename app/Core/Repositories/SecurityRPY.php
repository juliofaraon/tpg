<?php

namespace App\Core\Repositories;

use DB;
use Bouncer;
use Silber\Bouncer\Database\Role;
use App\Core\Eloquent\Security\NavigationUser;

class SecurityRPY
{
    public function forStoreNavigationUser($ip, $vPath, $user)
    {
        $objNavigationUser = new NavigationUser();
        $objNavigationUser->user_id = $user->id;
        $objNavigationUser->ip = $ip;
        $objNavigationUser->vpath = $vPath;
        $objNavigationUser->is_admin = $user->isAdmin();
        $objNavigationUser->save();
    }

    public function roles()
    {
        return Bouncer::role()->pluck('name', 'id')->toArray();
    }

    public function permissionByRol($roleID)
    {
        return DB::table('bouncer_permissions')
        ->where('entity_id', $roleID)
        ->where('entity_type', '=', 'roles');
    }

    public function createUserOrAdminAndRole($obj, $data, $role, $flag)
    {
        $obj->fill($data);

        if ($flag) {
            $obj->status = 'A';
        }
        $obj->save();
        $obj->assign(Role::findOrFail($role));

        return $obj;
    }
}
