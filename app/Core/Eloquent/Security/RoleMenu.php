<?php

namespace App\Core\Eloquent\Security;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menus';
}
