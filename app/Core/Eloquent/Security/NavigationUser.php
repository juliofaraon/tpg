<?php

namespace App\Core\Eloquent\Security;

use App\Core\Traits\DateTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NavigationUser extends Model
{
    use DateTrait,SoftDeletes;
    protected $fillable = ['user_id', 'ip', 'vpath', 'is_admin'];
}
