<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavigationUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('navigation_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vpath', 100);
            $table->string('ip', 100);
            $table->integer('user_id');
            $table->integer('is_admin');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('navigation_users');
    }
}
