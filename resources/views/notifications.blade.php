@extends('layouts.app')
@section('titleOption',__('options.notifications'))

@section('content')
<div class="container">
        {!! $notifications->render() !!}
    <div class="row justify-content-center">
                     
        
        <div class="my-3 p-3 bg-white rounded box-shadow col-md-10">
        <h6 class="border-bottom border-gray pb-2 mb-0">@lang('options.notifications')</h6>
        <br/>
         @foreach ($notifications as $notification)    
        <div class="media text-muted pt-3">
          
            @if ($notification->is_new) 
                <a data-toggle="tooltip" data-placement="top" data-original-title=" @lang('notifications.check')" style="width: 32px" href="{{route('check-notification', $notification->id)}}" class="mr-2 btn waves-effect waves-light btn-xs btn-info"><i class="fas fa-eye"></i></a>
            @else 
                <a data-toggle="tooltip" data-placement="top" data-original-title="@lang('notifications.uncheck') " style="width: 32px" href="{{route('uncheck-notification', $notification->id)}}" class="mr-2 btn waves-effect waves-light btn-xs btn-danger"><i class="fas fa-eye-slash"></i></a>
            @endif
        
            <label class="media-body" style="margin-bottom:0px">
                <p class="pb-3 mb-0 small lh-125">
                    <label  class="d-block text-gray-dark">
                        <strong> <a class="@if ($notification->is_new) text-danger @else text-info @endif" href="{{$notification->redirect_url}}" > {{ $notification->title }}</a></strong>&nbsp;
                        {{ $notification->created_at->diffForHumans()}}
                    </label>
                    {{ $notification->description }}
                </p>
            </label>  
         </div>
        
        @endforeach
           
      </div>
    </div>
</div>
@endsection