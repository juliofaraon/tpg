<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{asset('/images/32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('/images/192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('/images/180x180.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('/images/270x270.png')}}">



    <meta name="description" content="{{__('MetaDescription')}}">
    <meta name="author" content="{{__('MetaAuthor')}}">
    <title>{{config('app.name')}}</title>
    <link href="/css/app.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1 class="text-info"> {{ $code }}</h1>
                <h3 class="text-uppercase">{{ $title }}</h3>
                <p class="text-muted m-t-30 m-b-30">{{ $slot }}</p>
                <a href="@admin {{route('admin_dashboard')}}  @else {{route('home')}} @endadmin" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">{{__('ReturnMain')}}</a> </div>
            <footer class="footer text-center">{{__('FooterPage',['year'=>getYear()])}}</footer>
        </div>
    </section>
</body>
</html>