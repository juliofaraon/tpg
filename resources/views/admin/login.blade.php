@extends('layouts.authenticable')

@section('content')

<section id="wrapper">
        <div class="login-register background-login">
            <div class="login-box card ribbon-wrapper">
                <div class="ribbon ribbon-bookmark  ribbon-danger">{{ __('Access',['name'=>'administradores']) }}</div>
                <div class="card-body">
                        <form class="form-horizontal form-material" method="POST" action="{{ route('admin.login') }}">
                                @csrf
                                <div class="form-group ">
                                    <div class="col-xs-12 text-center">
                                     <img class="box-title m-b-20" src="{{asset('/images/logo.png')}}" style="width: 100%"/>
                                    </div>
                                </div>

                                <div class="form-group ">
                                        <div class="col-xs-12">
                                            <input required id="email" type="email"  name="email" value="{{ old('email') }}" placeholder="{{__('E-Mail Address') }}"
                                                     class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"> 
                                                     @if ($errors->has('email'))
                                                        <span class="invalid-feedback">
                                                             <strong>{{ $errors->first('email') }}</strong>
                                                         </span>
                                                    @endif
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-xs-12">
                                            <input  placeholder="{{ __('Password') }}"
                                            id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> 
                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                </div>
                               
       
                                <div class="form-group">
                                        <div class="col-md-12">
                                                <strong class="d-block text-gray-dark">
                                                        <label class="text-success">
                                                             <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                             <label style="top:3px" for="checkbox-signup" class="check-box"></label>&nbsp; {{__('Remember Me')}}
                                                         </label>
                                                        </strong>
                                        </div>
                                  </div>
        
                              
        
                                <div class="form-group text-center m-t-20">
                                        <div class="col-xs-12">
                                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Login', ['name'=>'Admin']) }}</button>
                                        </div>
                                </div>

                        
                            </form>
                </div> 
              <p class="ribbon-content">{{__('FooterPage',['year'=>date('Y')])}}</p>
            </div>
        </div>
</section>
@endsection