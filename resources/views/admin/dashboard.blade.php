@extends('layouts.app')
@section('titleOption',__('Admin Panel'))
@section('content')
<div class="container">
    <div class="row justify-content-center">
       <div class="col-md-10">
           
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-info"><i class="fas fa-microchip"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['totalProcess']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.totalProcess')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-danger"><i class="fas fa-cogs"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['kernelVersion']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.kernelVersion')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-info"><i class="fas fa-clock"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['uptime']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.uptime')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-danger"><i class="fab fa-stack-exchange"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['loadAverage']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.loadAverage')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-info"><i class="fas fa-hdd"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['diskUse']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.diskUse')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-danger"><i class="fa fa-cog"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['memoryUtilization']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.memoryUtilization')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-info"><i class="fas fa-plug"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['uniqueConnections']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.uniqueConnections')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-danger"><i class="fas fa-plug"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['uniqueConnections']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.uniqueConnections')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round round-lg align-self-center round-info"><i class="fas fa-plug"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h4 class="m-b-0 font-light">{{$infoServer['totalConnections']}}</h4>
                                    <h5 class="text-muted m-b-0">@lang('labels.totalConnections')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
</div>
@endsection
