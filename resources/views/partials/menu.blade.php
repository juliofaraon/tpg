
<aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
                <div class="user-profile" style="background: url({{asset('images/userinfo.jpg')}}) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';" 
                        src="{{currentUser()->avatar}}" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" 
                        data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{currentUser()->name}}</a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <a class="dropdown-item" href="{{ route('changePicture') }}"><i class="fa fa-image"></i> @lang('labels.change-picture')</a>
                            <div class="dropdown-divider"></div> 
                            <a class="dropdown-item" href="{{ route('changePassword') }}"><i class="ti-key"></i> @lang('labels.change-password')</a>
                            <div class="dropdown-divider"></div>
                                 <a href="#"  onclick="event.preventDefault();document.getElementById('logout-form').submit();" 
                                    class="dropdown-item"><i class="fa fa-power-off"></i> @lang('Logout')</a>
                        </div>
                    </div>
                </div>

            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                        @foreach ($menus as $key => $item)
                        @if ($item['parent'] <> null)
                            @break
                        @endif
                        @include('partials.menu-item', ['item' => $item])
                        @endforeach  
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
        <!-- Bottom points-->
        <div class="sidebar-footer">
            <!-- item-->
            <a href="{{route('readme')}}" class="link" data-toggle="tooltip" title="@lang('Readme')"><i class="mdi mdi-help"></i></a>
            <!-- item-->
            <a href="{{route('getAllNotification')}}" class="link" data-toggle="tooltip" title="@lang('notifications.get-all')">
                <i class="mdi mdi-message"></i></a>
            <!-- item-->
            
            <a href="#"  onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"
             class="link" data-toggle="tooltip" title="@lang('Logout')"><i class="mdi mdi-power"></i></a>


        </div>
        <!-- End Bottom points-->
    </aside>


            