
@if ($item['submenu'] == [])
        <li>
            <a href="{{route('master',$item['route'])}}" class="waves-effect waves-dark"><i class="{{$item['icons']}}"></i>
                <span> {{ $item['name'] }}</span> </a>
        </li>
        @else
    
        <li>
            <a href="#" class="has-arrow waves-effect waves-dark" aria-expanded="false">
                    <i class="{{$item['icons']}}"></i> <span class="hide-menu">{{ $item['name'] }} </span>
                    </a>
            <ul aria-expanded="false" class="collapse">
                @foreach ($item['submenu'] as $submenu)
                    @if ($submenu['submenu'] == [])
                        <li>
                            <a href="{{route('master',$submenu['route'])}}">{{ $submenu['name'] }} </a></li>
                    @else
                        @include('partials.menu-item', [ 'item' => $submenu ])
                    @endif
                @endforeach
            </ul>
        </li>
@endif