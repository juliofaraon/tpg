<ul class="treeview">
@foreach ($menus as $key => $item)
                    @if ($item['parent'] <> 0)
                        @break
                    @endif
                    @include('partials.menu-item-tree', ['item' => $item])
@endforeach
</ul>


