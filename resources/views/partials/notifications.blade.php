


        <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
            @if((currentUser()->unreadNotifications->count() > 0))
           <div class="notify" > <span class="heartbit"></span> <span class="point"></span> </div>
           @endif
        </a>
        @if(currentUser()->notifications->count()>0)
            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                    <ul>
                            <li>
                                <div class="drop-title">{{ __('notifications.recieve')}}</div>
                            </li>
                            <li>
                                <div class="message-center">
                                @foreach( currentUser()->notifications->take(4) as $notification)
                                    <a href="{{ $notification->url }}">
                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                        <div class="mail-contnet"  @if($notification->is_new) style="font-weight:bold" @endif>
                                            <h5>{{ $notification->title }}</h5> <span class="mail-desc">{{ strMaskData($notification->description) }}</span> 
                                            <span class="time">{{ $notification->created_at->diffForHumans() }} </span> </div>
                                    </a>
                                    @endforeach
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center" href="{{route('getAllNotification')}}"> <strong>{{ __('notifications.get-all')}}</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                    </ul>          
            </div>
        @endif