<div id="field_{{ $id }}" class="form-group">
    <label for="{{ $id }}"{!! Html::classes(['text-danger' => $hasErrors]) !!}>
        {{ $label }}
@if ($required)
        <span {!!Html::classes([config('html.themes.required')])!!} >@lang('Required')</span>
@endif
    </label>
    {!! $input !!}
@foreach ($errors as $error)
    <div class="invalid-feedback">{{ $error }}</div>
@endforeach
</div>