<h4{!! Html::classes(['text-danger' => $hasErrors]) !!}>
    {{ $label }}
@if ($required)
<span {!!Html::classes([config('html.themes.required')])!!} >@lang('Required')</span>
@endif
</h4>
{!! $input !!}
@foreach ($errors as $error)
    <p class="text-danger">{{ $error }}</p>
@endforeach

<br>