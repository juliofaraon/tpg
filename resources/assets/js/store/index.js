import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import apiModule from './modules/FunctionAPI';
import backModule from './modules/FunctionBackGround'

export default new Vuex.Store({
	
	modules:{
		apiModule,backModule
	}
});