<?php

return [
    'contact-mail-notification' => 'Mensaje de :name',
    'recieve' => 'Notificaciones Recibidas',
    'check-all' => 'Marcar todas como leída',
    'get-all' => 'Ver todas las notificaciones',
    'check' => 'Marcar como leído',
    'uncheck' => 'Marcar como no leído',
    'send' => 'Notificación enviada correctamente',
];
