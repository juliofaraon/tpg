<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminLogoutTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_admin_can_logout()
    {
        auth('admin')->login($this->createAdmin());
        $this->assertAuthenticated('admin');
        $response = $this->post('core-admin/logout');
        $response->assertRedirect('/');
        $this->assertGuest('admin');
    }

    /** @test */
    public function llogging_out_as_an_admin_does_not_terminate_the()
    {
        auth('admin')->login($this->createAdmin());
        auth('web')->login($this->createUser());

        $adminSessionName = auth('admin')->getName();
        $webSessionName = auth('web')->getName();

        $this->assertAuthenticated('admin');
        $this->assertAuthenticated('web');

        $response = $this->post('core-admin/logout');
        $response->assertRedirect('/')->assertSessionHas($webSessionName)->assertSessionMissing($adminSessionName);

        $this->assertGuest('admin');
        $this->assertAuthenticated('web');
    }
}
